import {inject, injectable} from 'inversify'
import {Cookie, CookieService} from '../cookies/cookies-service'
import {TYPES} from '../inverisfy/types'
import {ApiConfig} from '../config'
import {JWTCookieFactory} from './token-factory'

export interface TokenService {
    getAccessToken(): string

    getRefreshToken(): string

    setAccessToken(access: string): void

    setRefreshToken(refresh: string): void

    removeAccessToken(): void

    removeRefreshToken(): void
}

@injectable()
export class CookieTokenService implements TokenService {
    private cookieService: CookieService
    private jwtCookieFactory: JWTCookieFactory

    constructor(@inject(TYPES.CookiesService) cookieService: CookieService,
                @inject(TYPES.JWTCookiesFactory) cookieFactory: JWTCookieFactory) {
        this.cookieService = cookieService
        this.jwtCookieFactory = cookieFactory
    }

    getAccessToken() {
        return this.cookieService.getCookie(ApiConfig.ACCESS_TOKEN_NAME).value
    }

    getRefreshToken() {
        return this.cookieService.getCookie(ApiConfig.REFRESH_TOKEN_NAME).value
    }

    removeAccessToken() {
        this.cookieService.eraseCookie(ApiConfig.ACCESS_TOKEN_NAME)
    }

    removeRefreshToken() {
        this.cookieService.eraseCookie(ApiConfig.REFRESH_TOKEN_NAME)
    }

    setAccessToken(access: string) {
        const refreshCookie: Cookie =
            this.jwtCookieFactory(ApiConfig.ACCESS_TOKEN_NAME).createCookie(access)
        this.cookieService.setCookie(refreshCookie)
    }

    setRefreshToken(refresh: string) {
        const refreshCookie: Cookie =
            this.jwtCookieFactory(ApiConfig.REFRESH_TOKEN_NAME).createCookie(refresh)
        this.cookieService.setCookie(refreshCookie)
    }
}
