import {ApiConfig} from '../config'
import {Cookie} from '../cookies/cookies-service'

const {ACCESS_TOKEN_NAME, ACCESS_TOKEN_LIFETIME_MINUTES, REFRESH_TOKEN_NAME, REFRESH_TOKEN_LIFETIME_DAYS} = ApiConfig

abstract class CookieFactory {
    abstract createCookie(value: string): Cookie
}


class AccessTokenCookieFactory extends CookieFactory {
    createCookie(value): Cookie {
        const minutesToMilliseconds = 60 * 1000
        return {
            name: ACCESS_TOKEN_NAME,
            value: value,
            options: {
                expiresInMilliseconds:
                    ACCESS_TOKEN_LIFETIME_MINUTES * minutesToMilliseconds
            }
        }
    }
}

class RefreshTokenCookieFactory extends CookieFactory {
    createCookie(value): Cookie {
        const daysToMilliseconds = 24 * 60 * 60 * 1000
        return {
            name: REFRESH_TOKEN_NAME,
            value: value,
            options: {
                expiresInMilliseconds: REFRESH_TOKEN_LIFETIME_DAYS * daysToMilliseconds
            }
        }
    }
}

const FACTORIES = {
    [ACCESS_TOKEN_NAME]: new AccessTokenCookieFactory(),
    [REFRESH_TOKEN_NAME]: new RefreshTokenCookieFactory()
}

type CookieFactoryType = keyof typeof FACTORIES

export const getCookieFactory = (type: CookieFactoryType) => {
    return FACTORIES[type]
}
export type JWTCookieFactory = typeof getCookieFactory
