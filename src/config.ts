export const ApiConfig = {
    API_URL: 'http://127.0.0.1:8000/api/v1/',
    ACCESS_TOKEN_NAME: 'access',
    REFRESH_TOKEN_NAME: 'refresh',
    ACCESS_TOKEN_LIFETIME_MINUTES: 15,
    REFRESH_TOKEN_LIFETIME_DAYS: 1
}
