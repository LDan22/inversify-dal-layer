import {container} from './inversify.config'
import {Api} from '../api'
import {TYPES} from './types'
import {CookieService} from '../cookies/cookies-service'

export const api = container.get<Api>(TYPES.Api)
export const cookiesService = container.get<CookieService>(TYPES.CookiesService)
