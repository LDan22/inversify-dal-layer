import {Container} from 'inversify'
import {TYPES} from './types'
import {AxiosClientFactory, ClientFactory} from '../client'
import {Api, ApiImpl} from '../api'
import CookieServiceImpl, {CookieService} from '../cookies/cookies-service'
import {getCookieFactory, JWTCookieFactory} from '../token/token-factory'
import {CookieTokenService, TokenService} from '../token/token-service'

const container = new Container()
container.bind<ClientFactory>(TYPES.ClientFactory).to(AxiosClientFactory)
container.bind<Api>(TYPES.Api).to(ApiImpl)
container.bind<Document>(TYPES.Document).toConstantValue(document)
container.bind<CookieService>(TYPES.CookiesService).to(CookieServiceImpl)
container.bind<JWTCookieFactory>(TYPES.JWTCookiesFactory).toFunction(getCookieFactory)
container.bind<TokenService>(TYPES.TokenService).to(CookieTokenService)
export {container}
