export const TYPES = {
    'Api': Symbol('Api'),
    'ApiOptions': Symbol('ApiOptions'),
    'ClientFactory': Symbol('ClientFactory'),
    'CookiesService': Symbol('CookiesService'),
    'Document': Symbol('Document'),
    'TokenService': Symbol('TokenService'),
    'JWTCookiesFactory': Symbol('JWTCookiesFactory')
}
