import {inject, injectable} from 'inversify'
import {TYPES} from '../inverisfy/types'

export interface Cookie {
    name: string
    value: string
    options?: {
        expiresInMilliseconds?: number
        path?: string
    }
}

export interface CookieService {
    getCookie(name: string): Cookie

    setCookie(cookie: Cookie): void

    eraseCookie(name: string): void
}

@injectable()
export default class CookieServiceImpl implements CookieService {
    private document: Document

    constructor(@inject(TYPES.Document) document: Document) {
        this.document = document
    }

    getCookie(name: string): Cookie {
        let cookieValue = this.getCookieValue(name)
        return {
            name: name,
            value: cookieValue
        }
    }

    setCookie(cookie: Cookie): void {
        this.document.cookie =
            cookie.name +
            '=' +
            cookie.value +
            this.getCookieExpiresString(cookie) +
            this.getCookiePathString(cookie)
    }

    eraseCookie(name: string): void {
        this.document.cookie = name + '=; Max-Age=-99999999;'
    }

    getCookiePathString(cookie: Cookie): string {
        if (cookie.options?.path) {
            return ';path=' + cookie.options.path
        }
        return ';path=/'
    }

    getCookieExpiresString(cookie: Cookie): string {
        if (cookie.options?.expiresInMilliseconds) {
            let d = new Date()
            d.setTime(d.getTime() + cookie.options.expiresInMilliseconds)
            return ';expires=' + d.toUTCString()
        }
        return ''
    }

    getCookieValue(name: string): string {
        let cookie = this.extractCookie(name)
        return cookie.substring(name.length + 1, cookie.length)
    }

    extractCookie(name: string): string {
        let decodedCookie = decodeURIComponent(this.document.cookie)
        let cookies = decodedCookie.split(';')
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i]
            while (cookie.charAt(0) === ' ') {
                cookie = cookie.substring(1)
            }
            if (cookie.indexOf(name) === 0) {
                return cookie
            }
        }
        return ''
    }
}
