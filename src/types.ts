interface LoginData {
    email: string
    password: string
}

interface LoginResponse {
    access: string
    refresh: string
}

interface User {
    id: number
    email: string
    isActive: boolean
    isStaff: boolean
    isSuperuser: boolean
}

interface Profile {
    id: number
    bio: string
    emailVerified: boolean
    profilePic: string
}

interface UserProfile extends User {
    profile: Profile
}
