import {AxiosError, AxiosInstance, AxiosPromise, AxiosRequestConfig, AxiosResponse} from 'axios'
import {inject, injectable} from 'inversify'
import {TYPES} from './inverisfy/types'
import {ClientFactory} from './client'
import {TokenService} from './token/token-service'

export interface Api {
    client: AxiosInstance

    login(loginData: LoginData): Promise<void>

    logout(): Promise<void>

    me(): Promise<UserProfile>
}

declare module 'axios' {
    export interface AxiosRequestConfig {
        retry?: boolean
    }
}

@injectable()
export class ApiImpl implements Api {
    client: AxiosInstance
    protected access: string
    protected refresh: string
    refreshRequest: AxiosPromise | null
    tokenService: TokenService

    constructor(@inject(TYPES.ClientFactory) clientFactory: ClientFactory,
                @inject(TYPES.TokenService) tokenService: TokenService) {
        this.client = clientFactory.createClient()
        this.tokenService = tokenService
        this.access = this.tokenService.getAccessToken()
        this.refresh = this.tokenService.getRefreshToken()
        this.useInterceptors()
    }

    useInterceptors() {
        this.useRequestInterceptors()
        this.useResponseInterceptors()
    }

    useRequestInterceptors() {
        this.client.interceptors.request.use(
            (config: AxiosRequestConfig) =>
                this.onFulfilledRequestInterceptor(config),
            (e: AxiosError) => this.onRejectedRequestInterceptor(e)
        )
    }

    onFulfilledRequestInterceptor(config: AxiosRequestConfig) {
        if (!this.access) {
            return config
        }

        const newConfig = {
            headers: {},
            ...config
        }

        newConfig.headers.Authorization = `Bearer ${this.access}`
        return newConfig
    }

    onRejectedRequestInterceptor(error: AxiosError) {
        return Promise.reject(error)
    }

    useResponseInterceptors() {
        this.client.interceptors.response.use(
            (r: AxiosResponse) => this.onFulfilledResponseInterceptor(r),
            async (error: AxiosError) => this.onRejectedResponseInterceptor(error)
        )
    }

    onFulfilledResponseInterceptor(response: AxiosResponse) {
        return response
    }

    async onRejectedResponseInterceptor(error: AxiosError) {
        if (!this.refresh || error.response?.status !== 401 || error.config.retry) {
            return Promise.reject(error)
        }

        // if occurs 401 error on refresh request
        if (this.refreshRequest) {
            this.refreshRequest = null
            return Promise.reject(error)
        }

        const data = await this.makeRefreshRequest()
        this.refreshRequest = null
        this.access = data.access
        this.tokenService.setAccessToken(data.access)
        const newRequestConfig: AxiosRequestConfig = {
            ...error.config,
            retry: true
        }
        return this.client(newRequestConfig)
    }

    async makeRefreshRequest() {
        this.refreshRequest = this.client.post('/auth/token/refresh/', {
            refresh: this.refresh
        })
        const {data} = await this.refreshRequest
        return data
    }

    async login(loginData: LoginData) {
        const {data} = await this.client.post<LoginResponse>('auth/token/', loginData)
        this.access = data.access
        this.refresh = data.refresh
        this.tokenService.setAccessToken(data.access)
        this.tokenService.setRefreshToken(data.refresh)
    }

    async logout() {
        await this.client.post('auth/logout/', {refresh: this.refresh})
        this.access = ''
        this.refresh = ''
        this.tokenService.removeAccessToken()
        this.tokenService.removeRefreshToken()
    }

    async me() {
        const {data} = await this.client.get<UserProfile>('auth/me/')
        return data
    }

}
