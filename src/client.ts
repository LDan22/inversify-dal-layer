import axios, {AxiosInstance} from 'axios'
import {injectable} from 'inversify'
import {ApiConfig} from './config'

export interface ClientFactory {
    createClient: () => AxiosInstance
}

@injectable()
export class AxiosClientFactory implements ClientFactory {

    createClient(): AxiosInstance {
        return axios.create({
            baseURL: ApiConfig.API_URL
        })
    }
}
