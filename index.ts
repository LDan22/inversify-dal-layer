import 'reflect-metadata'
import {api, cookiesService} from './src/inverisfy/composition-root'


export {
    api,
    cookiesService
}
