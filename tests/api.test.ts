import 'reflect-metadata'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import {Api} from '../src/api'
import {container} from '../src/inverisfy/inversify.config'
import {TYPES} from '../src/inverisfy/types'

describe('Test api ', () => {
    Object.defineProperty(document, 'cookie', {
        get: jest.fn().mockImplementation(() => {
            return ''
        }),
        set: jest.fn().mockImplementation(() => {
        })
    })
    let mock: MockAdapter
    let api: Api
    beforeEach(() => {
        mock = new MockAdapter(axios)
        api = container.get(TYPES.Api)
    })

    const LOGIN_REQUEST = {
        email: 'foo@email.com',
        password: 'foo'
    }
    const LOGIN_RESPONSE = {
        access: 'TOKEN',
        refresh: 'REFRESH_TOKEN'
    }

    const REFRESH_REQUEST = {
        refresh: LOGIN_RESPONSE.refresh
    }
    const REFRESH_RESPONSE = {
        access: 'TOKEN2'
    }

    test('Login captures token information', async () => {
        mock.onPost('/auth/token/', LOGIN_REQUEST).reply(200, LOGIN_RESPONSE)
        mock.onGet('/auth/me/').reply(200, {})
        await api.login(LOGIN_REQUEST)
        await api.me()
        expect(mock.history.get.length).toBe(1)
        expect(mock.history.get[0].headers.Authorization).toBe(`Bearer ${LOGIN_RESPONSE.access}`)
    })

    test('Logout removes token information', async () => {
        mock.onPost('/auth/token/', LOGIN_REQUEST).reply(200, LOGIN_RESPONSE)
        mock.onPost('/auth/logout/').reply(200, {})
        mock.onGet('/auth/me/').reply(200, {})

        await api.login(LOGIN_REQUEST)
        await api.logout()
        await api.me()

        expect(mock.history.get.length).toBe(1)
        expect(mock.history.get[0].headers.Authorization).toBeFalsy()
    })

    test('Correctly retries request when got 401 with new token', async () => {
        mock.onPost('/auth/token/', LOGIN_REQUEST).reply(200, LOGIN_RESPONSE)
        mock
            .onPost('/auth/token/refresh/', REFRESH_REQUEST)
            .replyOnce(200, REFRESH_RESPONSE)
        mock.onGet('/auth/me/').reply(config => {
            const {Authorization: auth} = config.headers
            if (auth === `Bearer ${LOGIN_RESPONSE.access}`) {
                return [401]
            }
            if (auth === `Bearer ${REFRESH_RESPONSE.access}`) {
                return [200, {}]
            }
            return [404]
        })

        await api.login(LOGIN_REQUEST)
        await api.me()
        expect(mock.history.get.length).toBe(2)
        expect(mock.history.get[1].headers.Authorization).toBe(`Bearer ${REFRESH_RESPONSE.access}`)
    })

    test('Correctly fails when got non-401 error', async () => {
        mock.onGet('/auth/me/').reply(404)
        await expect(api.me()).rejects.toThrow()
    })

    test('Does not consume token more than once', async () => {
        mock.onPost('/auth/token/', LOGIN_REQUEST).reply(200, LOGIN_RESPONSE)
        mock
            .onPost('/auth/token/refresh/', REFRESH_REQUEST)
            .replyOnce(200, REFRESH_RESPONSE)

        mock.onGet('/auth/me/').reply(config => {
            const {Authorization: auth} = config.headers
            if (auth === `Bearer ${LOGIN_RESPONSE.access}`) {
                return [401]
            }
            if (auth === `Bearer ${REFRESH_RESPONSE.access}`) {
                return [200, {}]
            }
            return [404]
        })
        await api.login(LOGIN_REQUEST)

        await expect(Promise.all([api.me(), api.me()])).rejects.toThrow()
        expect(mock.history.post.filter(({url}) => url === '/auth/token/refresh/').length).toBe(1)
    })
})


